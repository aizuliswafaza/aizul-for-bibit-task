var express = require('express');
var router = express.Router();
const indexService = require("../services/indexService");

/* GET home page. */
router.get('/', indexService.index);

module.exports = router;