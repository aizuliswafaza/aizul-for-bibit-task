var express = require('express');
var router = express.Router();
const movieServiceV2 = require("../services/moviewServiceV2");
const logMiddleware = require('../middleware/LogMiddleware')

router.get('/detail', logMiddleware.set, movieServiceV2.detail);
router.get('/detail/:id', logMiddleware.set, movieServiceV2.findById);
router.get('/search', logMiddleware.set, movieServiceV2.search);

module.exports = router;
