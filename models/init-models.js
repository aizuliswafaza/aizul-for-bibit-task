var DataTypes = require("sequelize").DataTypes;
var _MstLogger = require("./mst_logger");

function initModels(sequelize) {
  var MstLogger = _MstLogger(sequelize, DataTypes);


  return {
    MstLogger,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
